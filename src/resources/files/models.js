/**
 * Imports
 */
import config from '../../config';
import {rethinkdb, Decorators as DBDecorators} from '../../core/db';
import {ValidationError} from '../../core/errors';

/**
 * Database tables
 */
const tables = {
    File: 'Files'
};

class File {
  /**
   * Create a new file
   */
  @DBDecorators.table(tables.File)
  static async create({sku, file}) {

      // Check if there is already a product with given SKU
      // if (await this.table.filter({sku}).count().run() > 0) {
      //     throw new ValidationError('sku', `SKU "${sku}" already in database`);
      // }

      // Insert product into database
      let obj = {
          file,
          collections: [],
          metadata: {},
          createdAt: new Date()
      };
      let insert = await this.table.insert(obj).run();

      // Get product object and return it
      return await this.table.get(insert.generated_keys[0]).run();
  }

  /**
   * Return products collection
   */
  @DBDecorators.table(tables.File)
  static async all() {

      // Build query
      let query = this.table;

      // Count the number of items that match query
      let count = await query.count().run();

      // Paginated query
      // if (perPage !== null && page !== null) {
      //     query = query.skip(page*perPage).limit(perPage);
      // }

      // Execute query
      let items = await query.run();

      // Return
      return {
          items: items,
          count: count
      }
  }

  @DBDecorators.table(tables.File)
  static async delete(id) {

      // Build query
      let query = this.table.get(id).delete();

      // Execute query
      let result = await query.run();

      // Return
      return {
          result: result
      }
  }

}

/**
 * Exports
 */
export {tables, File};

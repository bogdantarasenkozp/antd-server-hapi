/**
 * Imports
 */
import Joi from 'joi';

// API endpoint handlers
import {FileHandlers} from './handlers';

/**
 * Routes
 */
export default [
    {
        path: '',
        method: 'POST',
        config: {
            handler: {async: FileHandlers.post},
            auth: {
                strategy: 'jwt',
                scope: ['admin']
            },
            description: 'Upload file',
            tags: ['api'],
            payload: {
                maxBytes: 10048576,
                output: 'stream',
                parse: true
            },
            validate: {
                headers: Joi.object({
                    'authorization': Joi.string().required()
                }).unknown(),
                payload: {
                    file: Joi.object().required(),
                    resource: Joi.string().required()
                }
            }
        }
    },
    {
        path: '/upload',
        method: 'POST',
        config: {
            handler: {async: FileHandlers.post},
            auth: {
                mode: 'try',
                strategy: 'jwt'
            },
            description: 'Upload file',
            tags: ['api'],
            payload: {
                maxBytes: 1000048576,
                output: 'stream',
                parse: true
            },
            validate: {
                // headers: Joi.object({
                //     'authorization': Joi.string().required()
                // }).unknown(),
                payload: {
                    file: Joi.object().required(),
                    resource: Joi.string().required()
                }
            }
        }
    },
    {
        path: '/create',
        method: 'POST',
        config: {
            handler: {async: FileHandlers.create},
            auth: {
                mode: 'try',
                strategy: 'jwt'
            },
            description: 'Write file data',
            tags: ['api'],
            validate: {
                // headers: Joi.object({
                //     'authorization': Joi.string().required()
                // }).unknown(),
                // payload: {
                //     file: Joi.object().required(),
                //     resource: Joi.string().required()
                // }
            }
        }
    },
    {
        path: '/remove',
        method: 'DELETE',
        config: {
            handler: {async: FileHandlers.remove},
            auth: {
                mode: 'try',
                strategy: 'jwt'
            },
            description: 'remove file',
            tags: ['api'],
            validate: {
                // headers: Joi.object({
                //     'authorization': Joi.string().required()
                // }).unknown(),
                // payload: {
                //     file: Joi.object().required(),
                //     resource: Joi.string().required()
                // }
            }
        }
    },
    {
        path: '/all',
        method: 'GET',
        config: {
            handler: {async: FileHandlers.all},
            auth: {
                mode: 'try',
                strategy: 'jwt'
            },
            description: 'remove file',
            tags: ['api'],
            validate: {
                // headers: Joi.object({
                //     'authorization': Joi.string().required()
                // }).unknown(),
                // payload: {
                //     file: Joi.object().required(),
                //     resource: Joi.string().required()
                // }
            }
        }
    }
];

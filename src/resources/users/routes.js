/**
 * Imports
 */
import Joi from 'joi';

// Data schemas
import {UserSerializer} from './serializers';

// API endpoint handlers
import {UsersHandler} from './handlers';

export default [
    {
        path: '',
        method: 'GET',
        config: {
            handler: {async: UsersHandler.get},
            auth: {
              mode: 'try',
              strategy: 'jwt'
            },
            description: 'Get users collection',
            tags: ['api'],
        }
    },
    {
        path: '',
        method: 'POST',
        config: {
            handler: {async: UsersHandler.post},
            auth: {
                mode: 'try',
                strategy: 'jwt'
            },
            description: 'Create new product',
            tags: ['api'],
        }
    },
    {
        path: '/{productId}',
        method: 'PUT',
        config: {
            handler: {async: UsersHandler.put},
            auth: {
                mode: 'try',
                strategy: 'jwt'
            },
            description: 'Update all product details',
            tags: ['api'],
        }
    },
    {
        path: '',
        method: 'DELETE',
        config: {
            handler: {async: UsersHandler.delete},
            auth: {
                mode: 'try',
                strategy: 'jwt'
            },
            description: 'Delete product',
            tags: ['api']
        }
    },
    {
        path: '/{userId}',
        method: 'GET',
        config: {
            handler: {async: UsersHandler.getOne},
            auth: {
              mode: 'try',
              strategy: 'jwt'
            },
            description: 'Get user by id collection',
            tags: ['api'],
        }
    },
];

/**
 * Imports
 */
import { User } from './models';
/**
 * API handler for Users collection endpoint
 */
class UsersHandler {
    /**
     * Process GET request
     * Return the user's collection
     */
    static async get(request, reply) {
      const query = request.query.name;
      let response = {
        success: true,
        message: "OK",
        status: 200,
        items: await User.find({name:query})
      }
      return reply(response);
    }

    static async getOne(request, reply) {
      const { userId } = request.params;
      let fetch = await User.get( userId );
      let response = {
        success: true,
        message: "OK",
        status: 200,
        items: fetch
      }
      console.log('here');
      console.log(response);
      return reply(response);
    }
    /**
     * Process POST request
     * Create new user collection
     */
    static async post(request, reply) {
        try {
            let user = await User.create(request.payload);
            let response = {
              success: true,
              message: "OK",
              status: 200,
              data: user
            }
            return reply(response).code(201);
        }  catch (err) {
            if (err.name === ErrorName.VALIDATION_ERROR) {
                return reply(BadRequest.invalidParameters('payload', {[err.param]: [err.message]})).code(400);
            } else {
                log.error(err, 'Unable to create product');
                return reply().code(500);
            }
        }
    }
    /**
     * Process PUT request
     * Update user collection
     */
    static async put(request, reply) {
        // Check if product with given ID exists
        let product = await User.get(request.params.productId);
        if (!product) {
            return reply().code(404);
        }
        // Update product
        try {
            let user = await User.update(request.payload);
            // console.log(request.payload);
            let response = {
              success: true,
              message: "OK",
              status: 200,
              data: user
            }
            return reply(response);
        } catch (err) {
            if (err.name === ErrorName.VALIDATION_ERROR) {
                return reply(BadRequest.invalidParameters('payload', {[err.param]: [err.message]})).code(400);
            } else {
                log.error(err, 'Unable to update product');
                return reply().code(500);
            }
        }
    }
    /**
     * Process DELETE request
     * Delete user collection
     */
    static async delete(request, reply) {
        try {
            let removedProduct = await User.delete(request.payload);
            let response = {
              success: true,
              message: "OK",
              status: 200,
              data: removedProduct
            }
            return reply(response).code(201);
        } catch (err) {
            if (err.name === ErrorName.VALIDATION_ERROR) {
                return reply(BadRequest.invalidParameters('payload', {[err.param]: [err.message]})).code(400);
            } else {
                log.error(err, 'Unable to create product');
                return reply().code(500);
            }
        }
    }
}
/**
 * Exports
 */
export {UsersHandler};

/**
 * Imports
 */
import Joi from 'joi';

// API endpoint handlers
import {
    ProductsHandler,
    ProductIdHandler,
    ProductsUploadHandler
} from './handlers';

/**
 * Routes
 */
export default [
    {
        path: '',
        method: 'GET',
        config: {
            handler: {async: ProductsHandler.get},
            auth: {
                mode: 'try',
                strategy: 'jwt'
            },
            description: 'Get products collection',
            tags: ['api'],
        }
    },
    {
        path: '',
        method: 'POST',
        config: {
            handler: {async: ProductsHandler.post},
            auth: {
                mode: 'try',
                strategy: 'jwt'
            },
            description: 'Create new product',
            tags: ['api'],
        }
    },
    {
        path: '',
        method: 'DELETE',
        config: {
            handler: {async: ProductsHandler.delete},
            auth: {
                mode: 'try',
                strategy: 'jwt'
            },
            description: 'Delete product',
            tags: ['api'],
        }
    },
    {
        path: '/{productId}',
        method: 'GET',
        config: {
            handler: {async: ProductIdHandler.get},
            auth: {
                mode: 'try',
                strategy: 'jwt'
            },
            description: 'Get product',
            tags: ['api'],
            validate: {
                headers: Joi.object({
                    'authorization': Joi.string().optional()
                }).unknown(),
                params: {
                    productId: Joi.string().required().description('the id for the product'),
                }
            }
        }
    },
    {
        path: '/{productId}',
        method: 'PUT',
        config: {
            handler: {async: ProductIdHandler.put},
            auth: {
                mode: 'try',
                strategy: 'jwt'
            },
            description: 'Update all product details',
            tags: ['api'],
        }
    },
    {
        path: '/{productId}',
        method: 'PATCH',
        config: {
            handler: {async: ProductIdHandler.patch},
            auth: {
                strategy: 'jwt',
                scope: ['admin']
            },
            description: 'Partial product update',
            tags: ['api'],
            validate: {
                headers: Joi.object({
                    'authorization': Joi.string().required()
                }).unknown(),
                params: {
                    productId: Joi.string().required().description('the id for the product'),
                }
            }
        }
    },
    {
        path: '/upload',
        method: 'POST',
        config: {
            handler: {async: ProductsUploadHandler.post},
            auth: {
                strategy: 'jwt',
                scope: ['admin']
            },
            description: 'Upload catalog information',
            notes: 'Product content CSV and bulk image operations can be done using this endpoint',
            tags: ['api'],
            payload: {
                output: 'stream',
                parse: true
            },
            validate: {
                headers: Joi.object({
                    'authorization': Joi.string().required()
                }).unknown(),
                payload: {
                    resource: Joi.string().required(),
                    file: Joi.object().optional(),
                    action: Joi.string().optional()
                }
            }
        }
    }
];

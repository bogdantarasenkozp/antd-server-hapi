/**
 * Imports
 */
import csv from 'fast-csv';
import log from './../logging';
import { Product }    from './../models';
import { ErrorName }  from './../../../core/errors';
import { BadRequest } from './../../../core/responses';
import { deleteAll as deleteAllFiles } from './../../files/utils';

/**
 * API handler for Products collection endpoint
 */
class ProductsHandler {

    /**
     * Process GET request
     * Return the products's collection
     */
    static async get(request, reply) {
        //
        // Only authenticated Admins can see products that are not enabled
        //
        let isAdmin = request.auth.credentials && request.auth.credentials.scope && request.auth.credentials.scope.indexOf('admin') !== -1;
        let enabled = !isAdmin;

        const query = request.query.name;
        const orderBy = request.query.address;
        const createTime = request.query.createTime;
        //
        // Pagination
        //
        let perPage = 200; // Default
        let page = 0; // Default (IMPORTANT: 0 internally corresponds to 1 in request!)

        if (request.query.perPage) {
            if (isNaN(parseInt(request.query.perPage))) {
                return reply(BadRequest.invalidParameters('query', {perPage: 'Must be an integer'})).code(400);
            } else if (parseInt(request.query.perPage) < 1 || parseInt(request.query.perPage) > perPage) {
                return reply(BadRequest.invalidParameters('query', {perPage: 'Invalid'})).code(400);
            } else {
                perPage = parseInt(request.query.perPage);
            }
        }

        if (request.query.page) {
            if (isNaN(parseInt(request.query.page))) {
                return reply(BadRequest.invalidParameters('query', {page: 'Must be an integer'})).code(400);
            } else if (parseInt(request.query.page) < 1) {
                return reply(BadRequest.invalidParameters('query', {page: 'Invalid'})).code(400);
            } else {
                page = parseInt(request.query.page) - 1; // Because page 1 requested is equivalent to 0 internally
            }
        }
        //
        // Sorting
        //
        let sort = null;
        if (orderBy && orderBy !== '') {
            if (['sku', '-sku', 'alphabetically', '-alphabetically', 'price', '-price', 'date', '-date'].indexOf(orderBy) === -1) {
                return reply(BadRequest.invalidParameters('query', {sort: 'Invalid'})).code(400);
            } else {
                sort = orderBy;
            }
        }

        let results = await Product.find({
            sku: query,
            name_EN: query,
            name_DE: query,
            page: page,
            perPage: perPage,
            sort: sort,
            createTime: createTime
          }, enabled);
        // Return
        return reply({
            success: true,
            message: "OK",
            status: 200,
            total: results.count,
            list:  results.items
        });
    }

    /**
     * Process POST request
     * Create a new product
     */
    static async post(request, reply) {
        try {
            let product = await Product.create(request.payload);
            let response = {
              success: true,
              message: "OK",
              status: 200,
              data: product
            }
            return reply(response).code(201);
        } catch (err) {
            if (err.name === ErrorName.VALIDATION_ERROR) {
                return reply(BadRequest.invalidParameters('payload', {[err.param]: [err.message]})).code(400);
            } else {
                log.error(err, 'Unable to create product');
                return reply().code(500);
            }
        }
    }
    /**
     * Process DELETE request
     * Remove product
     */
    static async delete(request, reply) {
        try {
            let removedProduct = await Product.delete(request.payload);
            let response = {
              success: true,
              message: "OK",
              status: 200,
              data: removedProduct
            }
            return reply(response).code(201);
        } catch (err) {
            if (err.name === ErrorName.VALIDATION_ERROR) {
                return reply(BadRequest.invalidParameters('payload', {[err.param]: [err.message]})).code(400);
            } else {
                log.error(err, 'Unable to create product');
                return reply().code(500);
            }
        }
    }

}

export default ProductsHandler;

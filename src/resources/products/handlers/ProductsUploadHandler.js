/**
 * Imports
 */
import csv from 'fast-csv';
import log from './../logging';
import { Product }    from './../models';
import { BadRequest } from './../../../core/responses';
import { hasKeys, hasValue } from './../../../core/utils';
import { deleteAll as deleteAllFiles } from './../../files/utils';

/**
 * API handler for Products Upload
 */
class ProductsUploadHandler {

    /**
     * Process POST request
     */
    static async post (request, reply) {

        // a) Process catalog upload
        if (request.payload.resource === 'catalog') {
            if (!request.payload.file) {
                return reply(BadRequest.invalidParameters('payload', {file: ['Required']})).code(400);
            }

            let products = [];
            let keys = ['sku', 'name', 'description', 'currency', 'vat', 'listPrice', 'retailPrice', 'stock', 'enabled'];
            let csvStream = csv({headers: true})
                .on('data', function (row) {
                    if (!hasKeys(row, keys, true)) {
                        log.warn({row}, 'Row does not have required keys');
                    } else if(!hasValue(row, keys)) {
                        log.warn({row}, 'Row does not have value for all required keys');
                    } else {
                        products.push({
                            enabled: row['enabled'].toLowerCase() === 'true',
                            sku: row['sku'],
                            name: {
                                en: row['name'],
                                pt: row['name']
                            },
                            description: {
                                en: row['description'],
                                pt: row['description']
                            },
                            pricing: {
                                currency: row['currency'],
                                list: parseFloat(row['listPrice']),
                                retail: parseFloat(row['retailPrice']),
                                vat: parseInt(row['vat'])
                            },
                            stock: parseInt(row['stock'])
                        });
                    }
                })
                .on('end', async function () {
                    return reply(await Product.processCatalogUpload(products)).code(201);
                })
                .on('error', function () {
                    return reply(BadRequest.invalidParameters('payload', {file: ['Invalid']})).code(400);
                });
            request.payload.file.pipe(csvStream);
        }

        // b) Process images upload
        else if (request.payload.resource === 'images') {
            if (request.payload.action !== 'clear') {
                return reply(BadRequest.invalidParameters('payload', {action: ['Invalid']})).code(400);
            }
            await deleteAllFiles('products');
            await Product.clearImages();
            return reply().code(201);
        }

        // c) Unknown upload type
        else {
            return reply(BadRequest.invalidParameters('payload', {resource: ['Invalid']})).code(400);
        }
    }
}

export default ProductsUploadHandler;

/**
 * Imports
 */
import csv from 'fast-csv';
import log from './../logging';
import { Product }    from './../models';
import { ErrorName }  from './../../../core/errors';
import { BadRequest } from './../../../core/responses';
import {ProductSerializer} from './../serializers';
import { deleteAll as deleteAllFiles } from './../../files/utils';

/**
 * API handler for Product ID endpoint
 */
class ProductIdHandler {

    /**
     * Process GET request
     */
    static async get(request, reply) {
        let product = await Product.get(request.params.productId);
        // Note: Only authenticated Admins can see products that are not enabled
        let isAdmin = request.auth.credentials && request.auth.credentials.scope && request.auth.credentials.scope.indexOf('admin') !== -1;
        if (product && (product.enabled === true || isAdmin)) {
            return reply(await new ProductSerializer(product).serialize());
        } else {
            return reply().code(404);
        }
    }

    /**
     * Process PUT request
     */
    static async put(request, reply) {

        // Check if product with given ID exists
        let product = await Product.get(request.params.productId);
        if (!product) {
            return reply().code(404);
        }

        // Update product
        try {
            product = await Product.update(request.params.productId, request.payload);
            let serialize = await new ProductSerializer(product).serialize();
            let response = {
              success: true,
              message: "OK",
              status: 200,
              data: serialize
            }
            return reply(response);
        } catch (err) {
            if (err.name === ErrorName.VALIDATION_ERROR) {
                return reply(BadRequest.invalidParameters('payload', {[err.param]: [err.message]})).code(400);
            } else {
                log.error(err, 'Unable to update product');
                return reply().code(500);
            }
        }
    }

    /**
     * Process PATCH request
     */
    static async patch(request, reply) {

        // Check if product with given ID exists
        let product = await Product.get(request.params.productId);
        if (!product) {
            return reply().code(404);
        }

        // Validate payload and make respective updates
        if (hasKeys(request.payload, ['images'])) {
            if (!Array.isArray(request.payload.images)) {
                return reply(BadRequest.invalidParameters('payload', {images: ['Must be an array']})).code(400);
            } else {
                product = await Product.updateImages(product.id, request.payload.images);
            }
        } else {
            return reply({message: 'Invalid payload'}).code(400);
        }

        // Return
        return reply(await new ProductSerializer(product).serialize());
    }
}

export default ProductIdHandler;

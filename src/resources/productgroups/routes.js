/**
 * Imports
 */
import Joi from 'joi';
// API endpoint handlers
import { ProductGroupHandler } from './handlers';
/**
 * Routes
 */
export default [
    {
        path: '',
        method: 'GET',
        config: {
            handler: {async: ProductGroupHandler.get},
            auth: {
                mode: 'try',
                strategy: 'jwt'
            },
            description: 'Get product groups collection',
            tags: ['api'],
        }
    },
    {
        path: '',
        method: 'POST',
        config: {
            handler: {async: ProductGroupHandler.post},
            auth: {
                mode: 'try',
                strategy: 'jwt'
            },
            description: 'Create new product group',
            tags: ['api'],
        }
    },
    {
        path: '/{productId}',
        method: 'PUT',
        config: {
            handler: {async: ProductGroupHandler.put},
            auth: {
                mode: 'try',
                strategy: 'jwt'
            },
            description: 'Update all product group details',
            tags: ['api'],
        }
    },
    {
        path: '',
        method: 'DELETE',
        config: {
            handler: {async: ProductGroupHandler.delete},
            auth: {
                mode: 'try',
                strategy: 'jwt'
            },
            description: 'Delete product group',
            tags: ['api'],
        }
    },
    {
        path: '/{groupId}',
        method: 'GET',
        config: {
            handler: {async: ProductGroupHandler.getOne},
            auth: {
                mode: 'try',
                strategy: 'jwt'
            },
            description: 'Get products by id',
            tags: ['api'],
        }
    },
];

/**
 * Imports
 */
import csv from 'fast-csv';
import log from './../logging';
import { productGroup } from './../models';
import { ErrorName }    from './../../../core/errors';
import { BadRequest }   from './../../../core/responses';

class ProductGroupHandler {
    /**
     * Process GET request
     * Return the products's group collection
     */
    static async get(request, reply) {
        // Only authenticated Admins can see products that are not enabled
        let isAdmin = request.auth.credentials && request.auth.credentials.scope && request.auth.credentials.scope.indexOf('admin') !== -1;
        let enabled = !isAdmin;

        const query = request.query.name;
        const orderBy = request.query.address;
        const createTime = request.query.createTime;
        // Pagination
        let perPage = 200; // Default
        let page = 0; // Default (IMPORTANT: 0 internally corresponds to 1 in request!)

        if (request.query.perPage) {
            if (isNaN(parseInt(request.query.perPage))) {
                return reply(BadRequest.invalidParameters('query', {perPage: 'Must be an integer'})).code(400);
            } else if (parseInt(request.query.perPage) < 1 || parseInt(request.query.perPage) > perPage) {
                return reply(BadRequest.invalidParameters('query', {perPage: 'Invalid'})).code(400);
            } else {
                perPage = parseInt(request.query.perPage);
            }
        }

        if (request.query.page) {
            if (isNaN(parseInt(request.query.page))) {
                return reply(BadRequest.invalidParameters('query', {page: 'Must be an integer'})).code(400);
            } else if (parseInt(request.query.page) < 1) {
                return reply(BadRequest.invalidParameters('query', {page: 'Invalid'})).code(400);
            } else {
                page = parseInt(request.query.page) - 1; // Because page 1 requested is equivalent to 0 internally
            }
        }
        // Sorting
        let sort = null;
        if (orderBy && orderBy !== '') {
            if (['sku', '-sku', 'alphabetically', '-alphabetically', 'price', '-price', 'date', '-date'].indexOf(orderBy) === -1) {
                return reply(BadRequest.invalidParameters('query', {sort: 'Invalid'})).code(400);
            } else {
                sort = orderBy;
            }
        }
        // Fetch items
        let results = await productGroup.find({
            sku: query,
            name_EN: query,
            name_DE: query,
            page: page,
            perPage: perPage,
            sort: sort,
            createTime: createTime
          }, enabled);
        // Return
        return reply({
            success: true,
            message: "OK",
            status: 200,
            total: results.count,
            list: results.items
        });
    }
    /**
     * Process POST request
     * Create a new product group
     */
    static async post(request, reply) {
        try {
            let product = await productGroup.create(request.payload);
            let response = {
              success: true,
              message: "OK",
              status: 200,
              data: product
            }
            return reply(response).code(201);
        } catch (err) {
            if (err.name === ErrorName.VALIDATION_ERROR) {
                return reply(BadRequest.invalidParameters('payload', {[err.param]: [err.message]})).code(400);
            } else {
                log.error(err, 'Unable to create product');
                return reply().code(500);
            }
        }
    }
    /**
     * Process DELETE request
     * Remove product group
     */
    static async delete(request, reply) {
        try {
            let product = await productGroup.delete(request.payload);
            let response = {
              success: true,
              message: "OK",
              status: 200,
              data: product
            }
            return reply(response).code(201);
        } catch (err) {
            if (err.name === ErrorName.VALIDATION_ERROR) {
                return reply(BadRequest.invalidParameters('payload', {[err.param]: [err.message]})).code(400);
            } else {
                log.error(err, 'Unable to create product');
                return reply().code(500);
            }
        }
    }
    /**
     * Process PUT request
     * Update product group
     */
    static async put(request, reply) {
        // Check if product with given ID exists
        let product = await productGroup.get(request.params.productId);
        if (!product) {
            return reply().code(404);
        }
        // Update product
        try {
            product = await productGroup.update(request.params.productId, request.payload);
            let response = {
              success: true,
              message: "OK",
              status: 200,
              data: product
            }
            return reply(response);
        } catch (err) {
            if (err.name === ErrorName.VALIDATION_ERROR) {
                return reply(BadRequest.invalidParameters('payload', {[err.param]: [err.message]})).code(400);
            } else {
                log.error(err, 'Unable to update product');
                return reply().code(500);
            }
        }
    }
    static async getOne(request,reply) {
      const { groupId } = request.params;
      let fetch = await productGroup.find({ groupId: groupId });
      // Return
      return reply({
          success: true,
          message: "OK",
          status: 200,
          total: fetch.count,
          list:  fetch.items
      });
    }
}

export default ProductGroupHandler;

 /**
 * Imports
 */
import config from '../../config';
import {rethinkdb, Decorators as DBDecorators} from '../../core/db';
import {ValidationError} from '../../core/errors';

import log from './logging';

/**
 * Database tables
 */
const tables = {
    productGroup  : 'productGroup'
};

/**
 * Product model
 */
class productGroup {

    /**
     * Create a new product
     */

    @DBDecorators.table(tables.productGroup)
    static async create({ key, sku, stock, description_DE, description_EN, enabled, name_DE, name_EN, pricing_currency, pricing_list, pricing_retail, pricing_vat, avatar }) {

        // Check if there is already a product with given SKU
        if (await this.table.filter({sku}).count().run() > 0) {
            throw new ValidationError('sku', `SKU "${sku}" already in database`);
        }

        // Insert product into database
        let obj = {
          key,
          sku,
          stock,
          avatar,
          description_DE,
          description_EN,
          enabled,
          name_DE,
          name_EN,
          pricing_currency,
          pricing_list,
          pricing_retail,
          pricing_vat,
          tags: [],
          collections: [],
          metadata: {},
          createdAt: new Date(),
        };
        let insert = await this.table.insert(obj).run();

        // Get product object and return it
        return await this.table.get(insert.generated_keys[0]).run();
    }

    @DBDecorators.table(tables.productGroup)
    static async delete({ id }) {
      let remove = this.table.get(id).delete().run();
      return remove;
    }

    /**
     * Return products collection
     */
    @DBDecorators.table(tables.productGroup)
    static async find({groupId=null, sku=null, name_EN=null, name_DE=null, perPage=null, page=null, sort=null, createTime=null}, enabled) {

        // Build query
        let query = this.table;
        // query.filter((enabled === true) ? {enabled: true} : {});
        if (sku) {
            query = query.filter({sku: sku});
        }
        console.log(groupId);
        if (groupId) {
          query = query.filter({ id: groupId });
        }
        // if (name_DE) {
        //   query = query.filter({name_DE: name_DE});
        // }

        if(createTime && createTime.length == 2) {
          let date1 = createTime[0].split("-");
          let date2 = createTime[1].split("-");
          let time1 = rethinkdb.time(date1[0],date1[1],date1[2], 'Z');
          let time2 = rethinkdb.time(date2[0],date2[1],date2[2], 'Z');

            // query = query.between(new Date(createTime[0]), new Date(createTime[1]));
            // query = query.filter({createTime:createTime});
            query
            .filter((row) => {
                return row("date")
                      .date()
                      .during(time1, time2, {rightBound: "closed"})
            })

        }

        // Sort
        if (sort) {
            switch (sort) {
                case 'sku':
                    query = query.orderBy(rethinkdb.asc('sku'));
                    break;
                case '-sku':
                    query = query.orderBy(rethinkdb.desc('sku'));
                    break;
                case 'alphabetically':
                    query = query.orderBy(rethinkdb.asc('name'));
                    break;
                case '-alphabetically':
                    query = query.orderBy(rethinkdb.desc('name'));
                    break;
                case 'price':
                    query = query.orderBy(rethinkdb.asc(rethinkdb.row('pricing')('retail')));
                    break;
                case '-price':
                    query = query.orderBy(rethinkdb.desc(rethinkdb.row('pricing')('retail')));
                    break;
                case 'date':
                    query = query.orderBy(rethinkdb.asc('createdAt'));
                    break;
                case '-date':
                    query = query.orderBy(rethinkdb.desc('createdAt'));
                    break;
                default:
                    break;
            }
        }

        // Count the number of items that match query
        let count = await query.count().run();

        // Paginated query
        if (perPage !== null && page !== null) {
            query = query.skip(page*perPage).limit(perPage);
        }

        // Execute query
        let items = await query.run();
        // Return
        return {
            items: items,
            count: count
        }
    }

    /**
     * Update product group
    **/
    @DBDecorators.table(tables.productGroup)
    static async update(productId, { key, sku, stock, description_DE, description_EN, enabled, name_DE, name_EN, pricing_currency, pricing_list, pricing_retail, pricing_vat, }) {
console.log('her');
        // Validate that SKU does not belong to another product
        if (sku) {
            let product = await productGroup.getBySKU(sku);
            if (product && product.id !== productId) {
                throw new ValidationError('sku', `The SKU "${sku}" is already registered with another product`);
            }
        }

        // Update product
        await this.table.get(productId).update({
          key,
          sku,
          stock,
          description_DE,
          description_EN,
          enabled,
          name_DE,
          name_EN,
          pricing_currency,
          pricing_list,
          pricing_retail,
          pricing_vat,
          tags: [],
          collections: [],
          metadata: {},
          createdAt: new Date(),
        }).run();

        // Fetch product's latest state and return.
        return await productGroup.get(productId);
    }

    /**
     * Return product with given ID
     */
    @DBDecorators.table(tables.productGroup)
    static async get(productId) {
        return await this.table.get(productId).run();
    }

    /**
     * Return products with given IDs
     */
    @DBDecorators.table(tables.Product)
    static async getAll(productIds) {
        return await this.table.getAll(...productIds).run();
    }

    /**
     * Return product with given SKU
     */
    @DBDecorators.table(tables.productGroup)
    static async getBySKU(sku) {

        // Filter database for products with given SKU
        let products = await this.table.filter({sku: sku}).run();

        // Result:
        // a) Single product matches SKU, return it
        // b) More than one product matches SKU, throw an error
        // c) No product matches SKU, return null
        if (products.length == 1) {
            return products[0];
        } else if (products.length > 1) {
            log.error({sku}, 'More than one product with same SKU');
            throw new ValidationError('sku', 'More than one product with same SKU');
        } else {
            return null;
        }
    }

    /**
     * Returns whether or not there is stock to fulfill the order
     * @param cart - the cart object
     */
    @DBDecorators.table(tables.Product)
    static async hasStock(cart) {
        let products = await Product.getAll(cart.products.map(function (product) { return product.id; }));
        let allProductsHaveStock = true;
        products.forEach(function (dbProduct) {
            let requestedQuantity = cart.products.filter(function (cartProduct) {
                return cartProduct.id === dbProduct.id
            })[0].quantity;
            if (dbProduct.stock < requestedQuantity) {
                allProductsHaveStock = false;
            }
        });
        return allProductsHaveStock;
    }

    /**
     * Update product images
     * @param productId - the product unique ID
     * @param images - an array of image objects (that contain URL and other info)
     * @returns the saved product object
     */
    @DBDecorators.table(tables.Product)
    static async updateImages(productId, images) {

        // Update product
        await this.table.get(productId).update({images, updatedAt: new Date()}).run();

        // Fetch product's latest state and return.
        return await Product.get(productId);
    }

    /**
     * Remove images from all products
     */
    @DBDecorators.table(tables.Product)
    static async clearImages() {
        return await this.table.update({images: [], updatedAt: new Date()}).run();
    }

    /**
     * Process catalog upload
     */
    @DBDecorators.table(tables.Product)
    static async processCatalogUpload(products) {

        let SKUs = products.map(p => p.sku);

        //
        // 1) Disable all products not in this list
        //
        log.debug('Disabling SKUs not in catalog');
        let disabledSKUs = await this.table.filter(function (product) {
            return rethinkdb.not(rethinkdb.expr(SKUs).contains(product('sku')));
        }).pluck('sku').run().map(p => p.sku);
        await this.table.filter(function (product) {
            return rethinkdb.not(rethinkdb.expr(SKUs).contains(product('sku')));
        }).update({enabled: false});
        log.debug({disabledSKUs}, 'Disabled SKUs');

        //
        // 2) Update products already in the database
        //
        log.debug('Updating existing products');
        let SKUsToUpdate = await this.table.filter(function (product) {
            return rethinkdb.expr(SKUs).contains(product('sku'))
        }).pluck('sku').run().map(p => p.sku);
        let productsToUpdate = products.filter(function (product) {
            return SKUsToUpdate.indexOf(product.sku) !== -1;
        });
        await * productsToUpdate.map(product => {
            this.table.filter({sku: product.sku}).update({
                enabled: product.enabled,
                pricing: product.pricing,
                stock: product.stock,
                updatedAt: new Date()
            }).run();
        });
        log.debug({SKUsToUpdate},'Updated');

        //
        // 3) Add new products
        //
        log.debug('Adding new products');
        let productsToAdd = products.filter(function (product) {
            return SKUsToUpdate.indexOf(product.sku) === -1;
        });
        await this.table.insert(productsToAdd.map(function (product) {
            return {
                enabled: product.enabled,
                sku: product.sku,
                name: product.name,
                description: product.description || {},
                images: [],
                pricing: product.pricing,
                stock: product.stock,
                tags: [],
                collections: [],
                metadata: {},
                createdAt: new Date()
            };
        })).run();
        log.debug({productsToAdd}, 'Products added');

        //
        // 4) Return stats
        //
        return {
            disabled: disabledSKUs,
            updated: SKUsToUpdate,
            added: productsToAdd.map(p => p.sku)
        };
    }
}

/**
 * Exports
 */
export {tables, productGroup};
